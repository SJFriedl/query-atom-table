/*
 * written by : Stephen J. Friedl
 *              Software Consultant
 *              Southern California USA
 *              steve@unixwiz.net
 *
 *  This code is in the public domain.
 *
 *  This program queries the key kernel parameters for the Global Atom Table
 *  and Global Registered Window Messages. Both are limited resources and are
 *  allocated across all processes in the given session. When they run out --
 *  at around 16k items - then no more windows can be created. This is bad.
 *
 *  Because this runs in the context of the session, running it as a user does
 *  show what's going on with the service, so we need to typically run it from
 *  the command line with the SysInternals "PSEXEC" command:
 *
 *      PSEXEC -s c:\bin\query-atom-table.exe
 *
 *  and it injects it into a service for us.
 *
 * COMMAND LINE
 * ------------
 *
 *	-version
 *
 *      Show brief version information (for now, just the build date) and exit
 *      with success status.
 *
 *	-verbose
 *
 *		Show all the detail we can find: all Window messages and all atoms.
 *
 *	-psoutput
 *
 *      Write variables suitable for processing by a Powershell reader. Looks like:
 *
 *          DELPHI  1
 *          ATOMS   252
 *          WMSGS   1731
 *
 *	    In practice we don't really care about the DELPHI or TOTAL counts, only the
 *	    individual ATOM and WMSGS counts, but we keep this around because some
 *	    software still uses it.
 *
 *  -logoutput
 *
 *	NOTE: in previous versions of this tool, window messages were not counted if
 *	there was a global atom with the same numeric value. They are different 
 *	namespaces, and this was clearly an error that under-reported the window
 *	message count.
 *
 *  NOTE: This tool was extracted from a larger system and cobbled together to run
 *  
 */

// We're all about the Unicode
#define UNICODE 1           // for Win32
#define _UNICODE 1          // for C Runtime

#define WIN32_LEAN_AND_MEAN      // Exclude rarely-used stuff from Windows headers

// From "targetver.h"; we still need to target very old operating systems
// (XP/Server 2003)  Sorry.

#define WINVER _WIN32_WINNT_WINXP
#define _WIN32_WINNT _WIN32_WINNT_WINXP
#define NTDDI_VERSION NTDDI_WINXP

#include <windows.h>
#include <stdio.h>
#include <tchar.h>
#include <stdlib.h>
#include <stddef.h>
#include <assert.h>
#include <stdarg.h>
#include <time.h>

// Quiet the compiler about variables we're not using
#define UNUSED_PARAMETER(x)     ( (void) (x) )
#define UNUSED_VARIABLE(x)      ( (void) (x) )

// How many *objects* are in the given array (not how mnay *bytes*)
#define COUNTOF(x)      ( sizeof(x) / sizeof((x)[0]) )

static size_t __stdcall getCurrentWindowStationName(wchar_t *obuf, size_t osize);
static void __cdecl die(const wchar_t *format, ...);

// Is this item (wmsg or atom) a *Delphi* item?
static inline bool isDelphi(const wchar_t *name)
{
    return _tcsncmp(name, L"ControlOfs", 10) == 0 || _tcsncmp(name, L"Delphi", 6) == 0;
}

int __cdecl wmain(int argc, wchar_t **argv)
{
    int  nStringAtoms= 0;
    int  nWmsgs = 0;
    int  nDelphiAtoms = 0;
    int  nDelphiWmsgs = 0;

    bool verbose    = false;
    bool delphionly = false;
    bool psoutput   = false;
    bool logoutput  = false;

    UNUSED_PARAMETER(argc);

    for ( argv++ ; *argv; argv++ )
    {
        if (_tcscmp(*argv, _T("-help")) == 0 || _tcscmp(*argv, _T("-?")) == 0)
        {
            static const TCHAR * const helpText[] = {
                _T("Usage: query-atom-table [options]"),
                _T(""),
                _T("  -help        Show this brief help listing"),
                _T("  -version     Show the program's build/version info"),
                _T("  -verbose     Show more runtime debugging"),
                _T("  -delphionly  Display all Dephi-related window messages"),
                _T("  -psoutput    Output everyting in a more or less parsable format"),
                _T("  -logoutput   Write output suitable for logging"),

                0 // ENDMARKER
            };

            for (const TCHAR *const *p = helpText; *p; p++)
                _putts(*p);

            exit(EXIT_SUCCESS);
        }

        else if (_tcscmp(*argv, _T("-version")) == 0)
        {
            wprintf(L"Unixwiz.net query-atom-table: built %s %s\n", _T(__DATE__), _T(__TIME__));

            exit(EXIT_SUCCESS);
        }
        else if (_tcscmp(*argv, _T("-verbose")) == 0)
            verbose = true;

        else if (_tcscmp(*argv, _T("-delphionly")) == 0)
            delphionly = true;

        else if (_tcscmp(*argv, _T("-psoutput")) == 0)
            psoutput = true;

        else if (_tcscmp(*argv, _T("-logoutput")) == 0)
            logoutput = true;

        else 
            die( _T("ERROR: \"%s\" is invalid cmdline param"), *argv);
    }

    if (verbose)
        wprintf( L"Unixwiz.net query-atom-table: built %s %s\n", _T(__DATE__), _T(__TIME__));

    // Find out our session info
    DWORD sessionid;
    
    if ( ! ProcessIdToSessionId( GetCurrentProcessId(), &sessionid ) )
            sessionid = (DWORD)-1;
    
    TCHAR wstnname[1024];

    if  ( getCurrentWindowStationName(wstnname, COUNTOF(wstnname)) == 0 )
        _tcscpy_s(wstnname, COUNTOF(wstnname), _T("-error-"));

    // ATOMS FIRST

    for (int i = 0xC000; i <= 0xFFFF; i++ )
    {
        TCHAR	name[1024];

        // Atoms go from 0..BFFF for integer atoms, strings from C000..FFFF.
        // We're only [apparently] concerned about the strings. We did try to
        // print the numeric atoms at one point, but it seems like every possible
        // numeric atom slot was in use with that number, so it wasn't really
        // providing anything actionable.

        if (GlobalGetAtomName( (ATOM) i, name, COUNTOF(name)) > 0)
        {
            nStringAtoms++;

            const bool d = isDelphi(name);

            if (d) nDelphiAtoms++;

            if (verbose || (d && delphionly))
                wprintf(L" 0x%04X; ATOM; %s%s\n", i, name, d ? L"  ***DELPHI" : L"" );
        }
    }

    // Now window messages

    for (int i = 0xC000; i <= 0xffff; i++ )
    {
    TCHAR name[1024];

        // These claim to be "clipboard formats" but in fact is the inverse
        // of RegisterWindowMessage - this is strange. This seems to be the
        // resource constraint in our application, so it's the one we mainly
        // care about.

        if (GetClipboardFormatName(i, name, COUNTOF(name)) > 0)
        {
            nWmsgs++;

            const bool d = isDelphi(name);

            if (d) nDelphiWmsgs++;

            if (verbose || (d && delphionly))
                wprintf(L" 0x%04X; WMSG; %s%s\n", i, name, d ? L"  ***DELPHI" : L"" );
        }
    }

    // Report totals

    if (logoutput)
    {
        char computername[256];
        DWORD n = sizeof computername;
        GetComputerNameA(computername, &n);

        time_t now;
        time(&now);

        struct tm tm;
        localtime_s(&tm, &now);

        char outbuf[1024];
        sprintf_s(outbuf, sizeof outbuf,
            "%04d-%02d-%02dT%02d:%02d:%02d\t%d\t%d\t%s",
            tm.tm_year + 1900,
            tm.tm_mon + 1,
            tm.tm_mday,
            tm.tm_hour,
            tm.tm_min,
            tm.tm_sec,
            nStringAtoms,
            nWmsgs, computername);

        puts(outbuf);
    }
    else if (psoutput)
    {
        _tprintf( _T("WINSTN\t%s\n"), wstnname);
        _tprintf( _T("ATOMS\t%d\n"), nStringAtoms);	// string only
        _tprintf( _T("WMSGS\t%d\n"), nWmsgs);
        _tprintf( _T("DELPHI\t%d\n"), nDelphiWmsgs);     // delphi window messages
    }
    else
    {
        _tprintf(_T("Session %d; %s; %d string atoms (%d Delphi); %d wmsgs (%d Delphi)\n"),
            sessionid,
            wstnname,
            nStringAtoms,
            nDelphiAtoms,
            nWmsgs,
            nDelphiWmsgs);
    }

    return 0;
}


// Fetch the current window station name; this is probably not that useful,
// but we have had it in the code forever when troubleshooting so there's
// no harm in leaving it in. We were mainly looking at behavior of the
// non-interactive session space. This is probably not that useful.

static size_t __stdcall getCurrentWindowStationName(wchar_t *obuf, size_t osize)
{
    assert(obuf != 0);
    assert(osize > 1);

    ZeroMemory(obuf, osize);

    HWINSTA wstn = GetProcessWindowStation();

    if (wstn == 0)
        return 0;

    if ( ! GetUserObjectInformationW(wstn, UOI_NAME, obuf, (DWORD)osize, nullptr) )
        return 0;

    return _tcslen(obuf);
}

// die()
//
//	Given a printf-style argument list, format it to the standard error stream,
//  add a newline, then exit the program with error status.
//	
static void __cdecl die(const wchar_t *format, ...)
{
    va_list args;

    va_start(args, format);
    vfwprintf(stderr, format, args);
    va_end(args);

    putwc('\n', stderr);
    exit(EXIT_FAILURE);
}