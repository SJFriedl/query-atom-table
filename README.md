query-atom-table
================

Written by : Stephen J Friedl / 
            Software Consultant /
            Southern California USA /
            steve@unixwiz.net

*This code is in the public domain*

This console program queries the global atom table (strings) and the global window message
table and reports on how full these tables are. Both are limited kernel resources shared
among the current desktop session space, and exhaustion is very painful.

For regular desktop sessions, you can log out and back in to get a fresh session,
but for programs running as a service under the LocalSystem account, there's no
solution other than to reboot.

This tool was originally written in ~2014 to research issues with global window
messages running out in a line-of-business application written in Delphi; this
seems to be a known issue in the VCL (Visual Control Library).

This directly queries the current session, so just running `query-atom-table` will
report the output:

```
PS> .\query-atom-table.exe
Session 2; WinSta0; 89 string atoms (0 Delphi); 545 wmsgs (4 Delphi)
```
but this is only useful when troubleshooting desktop apps running in the current
desktop session. We mostly deal with a service, so we use the SysInternals tool
`psexec` to run this in a service context, giving us access to the non-interactive
session space.

```
PS> psexec -s c:\bin\query-atom-table.exe
Session 0; Service-0x0-3e7$; 131 string atoms (60 Delphi); 689 wmsgs (405 Delphi)
```

Ref: https://docs.microsoft.com/en-us/sysinternals/downloads/psexec

Running with the `-verbose` options dumps the full contents of the global
string atom table and the global window message table, pointing out items
that appear to from the VCL.

There are other command line options, but they are mainly for my own
tooling and are probably not generally usefuil.

**Note** - this tool was written to troubleshoot a specific application,
so the features were driven to that specific goal, and the source was
extracted from a much larger suite of tools so it could be released in
simplified form. Some of the source constructs might look out of place.

## Other resources ##

There's a much more full-featured GUI tool available for graphically monitoring
these, Jordi Corbilla's atom table monitor:

https://github.com/JordiCorbilla/atom-table-monitor

and it's very well regarded. I haven't used it.

## Building ##

This program is written in C++ and compiles and builds with Visual Studio 2017,
which I believe is the last version that can target XP and Server 2003; I still
have to support these systems so am sticking with what works for me.

It builds 32-bit only (no benefit in 64-bit), and it requires the Visual Studio
2015 RC3 C runtime (which provides `VCRUNTIME140.dll`)

https://www.microsoft.com/en-us/download/details.aspx?id=52685

Only the 32-bit version is needed, and it only supports a Release build. If running
this complains about missing `
program 

I have given no thought whatsoever to building this *other than* under
Visual Studio.

## Download ##

An executable build from the code posted is available here:

http://unixwiz.net/tools/query-atom-table.exe

I promise that it was built directly from the source code provided, but do not
promise it has no bugs.

If it fails asking for `VCRUNTIME140.dll`, you need the MSVC Runtime noted above.
